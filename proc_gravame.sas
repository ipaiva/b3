/*--------------------------------------------------------------------------------------*/
/*--------------------------[ EXPLODE ]--------------------------------------------*/
/*--------------------------------------------------------------------------------------*/

data gravame_diario;
/*LENGTH */
/*	OPFI_CD_GRAVAME $ 30*/
/*	OPFI_CD_CONTRATO $ 30*/
/*	OPFI_NU_CPF_CNPJ_FINANCIADO $ 30*/
/*	OPFI_DS_NOME_FINANCIADO $ 30*/
/*	OPFI_NU_CNPJ_AF $ 30*/
/*	OPFI_DS_RAZAO_SOCIAL_AF $ 30*/
/*	OPFI_IN_TIPO_RESTRICAO $ 30*/
/*	OPFI_DT_INC_GRAVAME $ 30*/
/*;*/
SET METADADOS_OPFI 
( KEEP = 
	OPFI_CD_GRAVAME
	OPFI_CD_CONTRATO
	OPFI_NU_CPF_CNPJ_FINANCIADO
	OPFI_DS_NOME_FINANCIADO
	OPFI_NU_CNPJ_AF
	OPFI_DS_RAZAO_SOCIAL_AF
	OPFI_IN_TIPO_RESTRICAO
	OPFI_DT_INC_GRAVAME
);
DAT_CARGA = PUT(TODAY(),DATE9.);
DAT_MOD_REGISTRO = TODAY();
COD_STATUS_REGISTRO = 'A';

OPFI_CD_GRAVAME_TXT = PUT(OPFI_CD_GRAVAME,BEST.);
OPFI_NU_CPF_CNPJ_FINANCIADO_TXT = PUT(OPFI_NU_CPF_CNPJ_FINANCIADO,BEST.);
OPFI_NU_CNPJ_AF_TXT = PUT(OPFI_NU_CNPJ_AF,BEST.);
OPFI_IN_TIPO_RESTRICAO_TXT = PUT(OPFI_IN_TIPO_RESTRICAO,BEST.);

RENAME
	OPFI_CD_GRAVAME_TXT = PK_GRAVAME
	OPFI_CD_CONTRATO = PK_CONTRATO
	OPFI_NU_CPF_CNPJ_FINANCIADO_TXT = CPF_CNPJ_CONTA_PARTE
	OPFI_DS_NOME_FINANCIADO = NOME_SIMPL_PARTE
	OPFI_NU_CNPJ_AF_TXT = CPF_CNPJ_CONTRA_PARTE
	OPFI_DS_RAZAO_SOCIAL_AF = NOME_SIMPL_CONTRA_PARTE
	OPFI_IN_TIPO_RESTRICAO_TXT = MOT_GRAVAME
	OPFI_DT_INC_GRAVAME = DT_INCLUSAO_GRAVAME;
LABEL
	OPFI_CD_GRAVAME_TXT = "N�mero do gravame (restri��o)"
	OPFI_CD_CONTRATO = "N�mero do contrato (identifica��o do contrato na base de dados do agente financeiro)"
	OPFI_NU_CPF_CNPJ_FINANCIADO_TXT = "CPF ou CNPJ do cliente que efetuou o financiamento"
	OPFI_DS_NOME_FINANCIADO = "Nome do cliente que efetuou o financiamento"
	OPFI_NU_CNPJ_AF_TXT = "CNPJ do agente financeiro"
	OPFI_DS_RAZAO_SOCIAL_AF = "Raz�o social do agente financeiro"
	OPFI_IN_TIPO_RESTRICAO_TXT = "Tipo da restri��o financeira (gravame)"
	OPFI_DT_INC_GRAVAME = "Data de inclus�o do gravame";
DROP
	OPFI_CD_GRAVAME OPFI_NU_CPF_CNPJ_FINANCIADO OPFI_NU_CNPJ_AF OPFI_IN_TIPO_RESTRICAO;
RUN;


/*--------------------------------------------------------------------------------------*/
/*--------------------------[ Carga Di�ria ]--------------------------------------------*/
/*--------------------------------------------------------------------------------------*/

DATA gravame_diario;
SET UIDIA.gravame_diario;
	COD_STATUS_REGISTRO = "A";
	DAT_CARGA = put(TODAY(),date9.);
	DAT_CARGA_PROC = TODAY();
	DAT_MOD_REGISTRO = TODAY();
RUN;

proc sort data=gravame_diario; by pk_contrato ; run;
proc sort data=uif.gravame_hist; by pk_contrato ; run;


data UIFBKP.GRAVAME_HIST_BKP;
SET uif.gravame_hist;
RUN;

DATA uif.gravame_hist;
update uif.gravame_hist gravame_diario;
BY pk_contrato;
RUN;

%del_dataset(gravame_diario);