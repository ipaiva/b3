/*--------------------------------------------------------------------------------------*/
/*--------------------------[ Carga Di�ria ]--------------------------------------------*/
/*--------------------------------------------------------------------------------------*/

%valida_base(UIFDIA,OPERACAO_UIF_DIARIO);

DATA operacao_uif_diario;
RETAIN PK_OPERACAO PK_GRAVAME DAT_OPERACAO;
SET UIFDIA.operacao_uif_diario;
	DAT_OPERACAO = PUT(TODAY()-3,DATE9.);
	DT_OPERACAO = TODAY()-3;
	COD_STATUS_REGISTRO = "A";
/*	DAT_CARGA = put(TODAY(),date9.);*/
	DAT_CARGA = TODAY();
	DAT_MOD_REGISTRO = TODAY();
RUN;

data operacao_uif_diario;
set operacao_uif_diario (drop=DAT_OPERACAO);
RUN;

PROC SORT DATA=operacao_uif_diario;  by descending dt_operacao pk_gravame; RUN;



/*--------------------------------------------------------------------------------------*/
/*--------------------------[ CARGA PROCESSO ]------------------------------------------*/
/*--------------------------------------------------------------------------------------*/
DATA op_uif_proc;
SET uif.operacao_uif_hist ;
RUN;


PROC APPEND base=op_uif_proc force NOWARN DATA=operacao_uif_diario; RUN;
PROC SORT DATA=op_uif_proc;  by descending dt_operacao pk_gravame; RUN;




/*--------------------------------------------------------------------------------------*/
/*--------------------------[ MES ATUAL ]-----------------------------------------------*/
/*--------------------------------------------------------------------------------------*/
DATA op_mes_atual;
SET op_uif_proc;
	MES = month(dt_operacao);
WHERE DT_OPERACAO GE &DATA_o.;
RUN;
PROC SORT DATA=op_mes_atual; by descending dt_operacao pk_gravame; RUN;

proc sql;
	CREATE TABLE aux_op_mes_atual as 
		SELECT 
			PK_GRAVAME, 
			DT_OPERACAO, 
			DAT_CARGA,
			MES,
			count(PK_OPERACAO) as QTD_OPERACAO_MES,
			count(PK_OPERACAO) / &dias_tt_mes_o. AS MEDIA_OPERACOES_MES
		FROM op_mes_atual
		GROUP BY PK_GRAVAME, MES
		ORDER BY QTD_OPERACAO_MES desc;
QUIT;
PROC SORT DATA=aux_op_mes_atual;  by descending dt_operacao pk_gravame; RUN;


/*----------------------- DIAS N�O UTEIS --------------------------------- */




DATA op_mes_atual_nwd;
SET op_mes_atual;
WD = WEEKDAY(DT_OPERACAO);
/*IF WEEKDAY(DT_OPERACAO) IN (1,7) THEN OUTPUT;*/
WHERE WEEKDAY(DT_OPERACAO) IN (1,7) OR DT_OPERACAO IN (&vnwd.);
RUN;

PROC SQL;
	CREATE TABLE aux_op_mes_atual_nwd AS 
		SELECT 
			PK_GRAVAME,
			DT_OPERACAO,
			DAT_CARGA,
			MES,
			count(PK_OPERACAO) AS QTD_OPERACAO_MES_NUTIL,
			count(PK_OPERACAO) / &qty_nwd_o. AS MEDIA_OPERACOES_MES_NUTIL,
			&qty_nwd_o. AS qty_nwd_o
		FROM op_mes_atual_nwd
		GROUP BY PK_GRAVAME, MES
		ORDER BY QTD_OPERACAO_MES_NUTIL DESC;
QUIT;
PROC SORT DATA=aux_op_mes_atual_nwd; BY DESCENDING dt_operacao pk_gravame; RUN;



/*--------------------------------------------------------------------------------------*/
/*-------------------------------[ D90 ] -----------------------------------------------*/
/*--------------------------------------------------------------------------------------*/
DATA op_90;
SET op_uif_proc;
	IF DT_OPERACAO GE &DATA_90. AND DT_OPERACAO LT &data_o. THEN FL_D90 = 1;
	MES = month(dt_operacao);
	WHERE DT_OPERACAO GE &DATA_90.;
RUN;
PROC SORT DATA=op_90;  by descending dt_operacao pk_gravame; RUN;

PROC SQL;
	CREATE TABLE aux_op_90 as
		SELECT 
			PK_GRAVAME
			,FL_D90
			,MES
			,DT_OPERACAO
			,DAT_CARGA
			,SUM(FL_D90) as QTD_OPERACAO_D90
			,SUM(FL_D90)/3 as MEDIA_OPERACOES_D90
		FROM op_90
		GROUP BY PK_GRAVAME, FL_D90
		ORDER BY QTD_OPERACAO_D90;
quit;
PROC SORT DATA=aux_op_90;  by descending dt_operacao pk_gravame; RUN;


/*  ----------------------- DIAS N�O UTEIS --------------------------------- */

DATA op_90_nwd;
SET op_90;
WD = WEEKDAY(DT_OPERACAO);
/*IF WEEKDAY(DT_OPERACAO) IN (1,7) THEN OUTPUT;*/
WHERE WEEKDAY(DT_OPERACAO) IN (1,7) OR DT_OPERACAO IN (&vnwd_90.);
RUN;



PROC SQL;
	CREATE TABLE aux_op_mes_90_nwd as
		SELECT 
			PK_GRAVAME
			,FL_D90
			,MES
			,DT_OPERACAO
			,DAT_CARGA
			,sum(FL_D90) as QTD_OPERACAO_D90_NUTIL
			,SUM(FL_D90)/&qty_nwd_90. as MEDIA_OPERACOES_D90_NUTIL
			,&qty_nwd_90. as qty_nwd_90
		FROM op_90_nwd
		GROUP BY PK_GRAVAME, FL_D90
		ORDER BY QTD_OPERACAO_D90_NUTIL desc;
quit;

PROC SORT DATA=aux_op_mes_90_nwd; by descending dt_operacao pk_gravame; RUN;





/*--------------------------------------------------------------------------------------*/
/*-------------------------------[ FINALIZAR CARGA DI�RIA ] ----------------------------*/
/*--------------------------------------------------------------------------------------*/

DATA aux_op_mes_atual;
SET aux_op_mes_atual ;
WHERE DAT_CARGA EQ TODAY();
RUN;

/*----------------------- MERGE NWD ATUAL --------------------------------- */

PROC SORT DATA=aux_op_mes_atual;  BY pk_gravame DT_OPERACAO; RUN;
PROC SORT DATA=aux_op_mes_atual_nwd; BY pk_gravame DT_OPERACAO; RUN;

proc sort data=aux_op_mes_atual_nwd nodupkey; by PK_GRAVAME QTD_OPERACAO_MES_NUTIL MEDIA_OPERACOES_MES_NUTIL; run;

DATA cg_diaria_unif_1;
MERGE
	aux_op_mes_atual (in=a)
	aux_op_mes_atual_nwd (in=b KEEP=PK_GRAVAME QTD_OPERACAO_MES_NUTIL MEDIA_OPERACOES_MES_NUTIL);
	BY pk_gravame;
	IF A;
RUN;


/*----------------------- MERGE NWD 90 --------------------------------- */

PROC SORT DATA=aux_op_90; BY pk_gravame DT_OPERACAO ; RUN;
PROC SORT DATA=aux_op_mes_90_nwd; BY pk_gravame DT_OPERACAO; RUN;

data aux_op_90;
set aux_op_90;
by pk_gravame dt_operacao;
if first.pk_gravame and first.dt_operacao then output;
run;

data aux_op_mes_90_nwd;
set aux_op_mes_90_nwd;
by pk_gravame dt_operacao;
if first.pk_gravame and first.dt_operacao then output;
run;

/*DATA aux_op_90;*/
/*SET aux_op_90 (WHERE=(DT_OPERACAO LT &data_o.)) ;*/
/*RUN;*/
/**/
/*DATA aux_op_mes_90_nwd;*/
/*SET aux_op_mes_90_nwd (WHERE=(DT_OPERACAO LT &data_o.)) ;*/
/*RUN;*/

DATA cg_diaria_unif_2;
MERGE 
	aux_op_90 (in=a)
	aux_op_mes_90_nwd (in=b KEEP=PK_GRAVAME DT_OPERACAO QTD_OPERACAO_D90_NUTIL	MEDIA_OPERACOES_D90_NUTIL);
	BY PK_GRAVAME ;
	IF A;
RUN;



/*proc sort data=cg_diaria_unif_2 nodupkey; by PK_GRAVAME QTD_OPERACAO_D90 MEDIA_OPERACOES_D90 QTD_OPERACAO_D90_NUTIL MEDIA_OPERACOES_D90_NUTIL; run;*/

/*----------------------- MERGE ATUAL 90 --------------------------------- */

PROC SORT DATA=cg_diaria_unif_1;  BY pk_gravame; RUN;
PROC SORT DATA=cg_diaria_unif_2;  BY pk_gravame ; RUN;

DATA cg_diaria_unif (drop=MES);
MERGE
	cg_diaria_unif_1 (in=a)
	cg_diaria_unif_2 (in=b KEEP=PK_GRAVAME QTD_OPERACAO_D90 MEDIA_OPERACOES_D90 QTD_OPERACAO_D90_NUTIL MEDIA_OPERACOES_D90_NUTIL);
	BY pk_gravame;
	IF A;
RUN;

PROC SORT DATA=cg_diaria_unif; by PK_GRAVAME DT_OPERACAO; RUN;


/*--------------------------------------------------------------------------------------*/
/*-------------------------------[ HIST�RICO ] -----------------------------------------*/
/*--------------------------------------------------------------------------------------*/

PROC SORT DATA=op_uif_proc; BY PK_GRAVAME DT_OPERACAO ; RUN;
PROC SORT DATA=cg_diaria_unif; BY PK_GRAVAME DT_OPERACAO; RUN;

%del_dataset(CG_DIARIA_FINAL);

DATA cg_diaria_final;
MERGE 
	op_uif_proc (in=a)
	cg_diaria_unif (in=b);
	BY PK_GRAVAME DT_OPERACAO;
	IF A AND B;
run;

%update_flags(cg_diaria_final);

PROC SORT DATA=uif.OPERACAO_UIF_HIST; by DESCENDING DT_OPERACAO PK_GRAVAME; RUN;
PROC SORT DATA=cg_diaria_final; by DESCENDING DT_OPERACAO PK_GRAVAME; RUN;

/* TEST - not to overwrite the hist during tests */
/*data hist_uif_test;*/
/*set uif.OPERACAO_UIF_HIST;*/
/*run;*/
/*PROC APPEND BASE=hist_uif_test DATA=cg_diaria_final;RUN;*/
/*PROC SORT DATA=hist_uif_test; by descending DT_OPERACAO PK_GRAVAME ; RUN;*/
/* END TEST */

PROC APPEND BASE=uif.OPERACAO_UIF_HIST DATA=cg_diaria_final;RUN;

PROC SORT DATA=uif.operacao_uif_hist; by descending DT_OPERACAO PK_GRAVAME ; RUN;

/*--------------------------------------------------------------------------------------*/
/*-------------------------------[ EXPURGO ] -------------------------------------------*/
/*--------------------------------------------------------------------------------------*/

DATA uif.operacao_uif_hist UIFBKP.operacao_uif_ex;
SET uif.operacao_uif_hist;
IF DT_OPERACAO GE &DATA_ex. THEN OUTPUT uif.operacao_uif_hist;
ELSE OUTPUT UIFBKP.operacao_uif_ex;
RUN;

PROC SORT DATA=uif.operacao_uif_hist; by descending DT_OPERACAO PK_GRAVAME ; RUN;

proc print data=dates_V2;run;
proc print data=nwd;run;

/*%del_dataset(CG_DIARIA_FINAL);*/
%del_dataset(OPERACAO_UIF_DIARIO);
%del_dataset(OP_UIF_PROC);
%del_dataset(OP_MES_ATUAL);
%del_dataset(AUX_OP_MES_ATUAL);
%del_dataset(OP_MES_ATUAL_NWD);
%del_dataset(AUX_OP_MES_ATUAL_NWD);
%del_dataset(OP_90);
%del_dataset(AUX_OP_90);
%del_dataset(OP_90_NWD);
%del_dataset(AUX_OP_MES_90_NWD);
%del_dataset(CG_DIARIA_UNIF_1);
%del_dataset(CG_DIARIA_UNIF_2);
%del_dataset(CG_DIARIA_UNIF);
/*%del_dataset(OPERACAO_UIF_HIST);*/
/*%del_dataset(OPERACAO_UIF_EX);*/
