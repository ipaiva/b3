/* VEICULO */
data VEICULOS_DIARIO;
SET METADADOS_OPFI 
( KEEP =
	OPFI_DS_COD_TIPO_VEICULO
	OPFI_NU_RENAVAM_VEIC
	OPFI_DS_CHASSI_VEIC
	OPFI_DS_PLACA_VEIC
	OPFI_NU_ANO_FABRICACAO_VEIC
	OPFI_NU_VALOR_FIPE
	OPFI_DS_MARCA_MODELO_VEIC
	OPFI_DS_MARCA_MODELO_VEIC 
);
DAT_CARGA = PUT(TODAY(),DATE9.);
DAT_MOD_REGISTRO = TODAY();
COD_STATUS_REGISTRO = 'A';

OPFI_NU_VALOR_FIPE_TXT = PUT(OPFI_NU_VALOR_FIPE,BEST.);
OPFI_NU_ANO_FABRICACAO_VEIC_TXT = PUT(OPFI_NU_ANO_FABRICACAO_VEIC,BEST.);


RENAME
	OPFI_DS_COD_TIPO_VEICULO = PK_VEICULO /* CAMPO EST� ERRADO COM CERTEZA */
	OPFI_NU_RENAVAM_VEIC = RENAVAM
	OPFI_DS_CHASSI_VEIC = CHASSI
	OPFI_DS_PLACA_VEIC = PLACA
	OPFI_NU_ANO_FABRICACAO_VEIC_TXT = ANO
	OPFI_NU_VALOR_FIPE_TXT = VALOR
	OPFI_DS_MARCA_MODELO_VEIC = MODELO
	OPFI_DS_MARCA_MODELO_VEIC = FABRICANTE
;

LABEL
	OPFI_DS_COD_TIPO_VEICULO = "Identifica��o do tipo de ve�culo entre leve e pesado" /* CAMPO EST� ERRADO COM CERTEZA */
	OPFI_NU_RENAVAM_VEIC = "N�mero do renavam do ve�culo da opera��o"
	OPFI_DS_CHASSI_VEIC = "Chassi do ve�culo da opera��o"
	OPFI_DS_PLACA_VEIC = "Placa do ve�culo da opera��o"
	OPFI_NU_ANO_FABRICACAO_VEIC_TXT = "Ano de fabrica��o do ve�culo da opera��o"
	OPFI_NU_VALOR_FIPE_TXT = "M�dia aritm�tica do valor do ve�culo segundo tabela FIPE"
	OPFI_DS_MARCA_MODELO_VEIC = "Marca/ Modelo do ve�culo"
	OPFI_DS_MARCA_MODELO_VEIC = "Marca/ Modelo do ve�culo"
;

DROP
	OPFI_NU_VALOR_FIPE
	OPFI_NU_ANO_FABRICACAO_VEIC
;

RUN;


/*--------------------------------------------------------------------------------------*/
/*--------------------------[ Carga Di�ria ]--------------------------------------------*/
/*--------------------------------------------------------------------------------------*/

DATA veiculos_diario;
SET uifdia.veiculos_diario;
	COD_STATUS_REGISTRO = "A";
	DAT_CARGA = put(TODAY(),date9.);
	DAT_CARGA_PROC = TODAY();
	DAT_MOD_REGISTRO = TODAY();
RUN;

proc sort data=veiculos_diario; by id; run;
proc sort data=UIF.VEICULOS_CADASTRO; by id; run;

data UIFBKP.VEICULOS_CADASTRO_BKP;
SET uif.VEICULOS_CADASTRO;
RUN;

DATA UIF.VEICULOS_CADASTRO;
UPDATE UIF.VEICULOS_CADASTRO VEICULO_DIARIO;
BY ID;
RUN;

%del_dataset(veiculos_diario);