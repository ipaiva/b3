* +-------------------------------------------------------------------------------------+;
* | Name: Operacao_UIF.sas					                                            |;
* | Respons�vel: �caro Paiva								                            |;
* | Objetivo: Malha de processamento para ETL do produto OPERA��O UIF					|;
* | Data: 17/08/2021									  	 	                  		|;
* +-------------------------------------------------------------------------------------+;


/*--------------------------------------------------------------------------------------*/
/*--------------------------[ SAS initial settings ]------------------------------------*/
/*--------------------------------------------------------------------------------------*/
options compress=yes reuse=yes validvarname=v7
		SORTPGM=BEST ;


/*--------------------------------------------------------------------------------------*/
/*--------------------------[ LIBS ]----------------------------------------------------*/
/*--------------------------------------------------------------------------------------*/

LIBNAME B3 '\\172.16.137.41\coe-analitycs\bases\b3\cargas';
LIBNAME UIF '\\172.16.137.41\coe-analitycs\bases\b3';
LIBNAME UIFDIA '\\172.16.137.41\coe-analitycs\bases\b3\cg_diaria';
LIBNAME UIFBKP '\\172.16.137.41\coe-analitycs\bases\b3\uif_bkp';
LIBNAME PGRES (work);
LIBNAME S3 (work);


/*--------------------------------------------------------------------------------------*/
/*--------------------------[ MACROS ]--------------------------------------------------*/
/*--------------------------------------------------------------------------------------*/

%MACRO clean_ds(ds, col);
	DATA &ds.;
	SET &ds.;
	IF &col. EQ . THEN DELETE;
	RUN;
%mend;

%macro exist_dataset(LB, DS);
	%Put &DS.;
	%global ds_exist;
	data _null_; 
		dsname="&lb..&ds."; 
		Put dsname;
	  if (exist(dsname)) then call symput("ds_exist",1);
			else call symput("ds_exist",0);
	run;
	%Put NOTE- RESULT FOR &DS.: &ds_exist.;
%mend exist_dataset;

%macro valida_base(lb, ds);

	%exist_dataset(&LB.,&DS.);

	%IF &ds_exist. EQ 0 %THEN %DO;
	  %put ERROR: Carga di�ria n�o localizada ;
	  %return;
	%END;

%mend;

%macro update_flags(ds);
	data &ds.;
	set &ds.;
		flag_alertado = 1 ;
		flag_tratado = 0 ;
		resp_tratamento = "Em aberto" ;
	run;
%mend;

%macro clean_work();
	proc datasets nolist kill;
	quit;
%mend clean_work;

%macro del_dataset(base);
	proc datasets lib=WORK nolist;
		DELETE &base.;
	quit;
%mend del_dataset;


/*------------------------------------------------------------------------------*/
/*-----------------------------[ IMPORTS ]--------------------------------------*/
/*------------------------------------------------------------------------------*/

/*Import Metadados (dev) */
PROC IMPORT DATAFILE = "\\172.16.137.41\coe-analitycs\bases\b3\book1"
			DBMS = XLSX
			OUT = book1
			REPLACE;
			SHEET="METADADOS_OPFI";
RUN;

/* Import non-working days parameter sheet  */
PROC IMPORT DATAFILE = "\\172.16.137.41\coe-analitycs\bases\b3\book1"
			DBMS = XLSX
			OUT = PARAM
			REPLACE;
			SHEET="PARAMETERS";
RUN;

/* METADADOS create (dev) */
data METADADOS_OPFI;
SET book1;
run;

/* Mapping  non-working days from parameters sheet*/
DATA NWD;
SET PARAM (KEEP=DIAS_NUTIL);
	FORMAT DIAS_NUTIL DDMMYY10.;
	DIAS_PROC = DIAS_NUTIL;
RUN;
%clean_ds(NWD,dias_nutil);

/* Reading exp date from parametes sheet */
DATA _NULL_;
SET PARAM (KEEP=EXPURGO OBS=1);
	CALL SYMPUT("qty_month_exp", expurgo);
RUN;




/*--------------------------------------------------------------------------------------*/
/*-----------------------------[ MACRO VARI�VEIS ]--------------------------------------*/
/*--------------------------------------------------------------------------------------*/
/* 														  	 	                  		*/
/*--------------------------------------------------------------------------------------*/

data _null_;
	call symputx("data_o", intnx('month',today(),0,'b'));
	call symputx("data_90", intnx('month',today(),-3, 'b'));
/*	call symputx("data_90", intnx('day',today(),-90));*/
run;

data _null_;
format data_ex date9.;
	/*call symputx("mes_atual",month(today()));*/
	/*call symputx("ano_atual",year(today()));*/
	call symputx("dias_tt_mes_o", DAY(TODAY()));
	call symputx("data_ex", intnx('month',today(),-&qty_month_exp., 'b'));
	data_fim_90 = intnx('month',today(),-1, 'e');
	call symputx("data_fim_90",intnx('month',today(),-1, 'e'));
	data_ex = intnx('month',today(),-&qty_month_exp, 'b');
	qty_weekday_mes_o = intck('weekday', &data_o., today());
	qty_nwd_o = day(today()) - qty_weekday_mes_o;
	call symputx("qty_nwd_o", qty_nwd_o);
	qty_weekday_90 = intck('weekday', &data_90.,data_fim_90);
	qty_tt_days_90 = intck('day', &data_90.,data_fim_90);
	call symputx("qty_tt_days_90",qty_tt_days_90);
	qty_nwd_90 = qty_tt_days_90 - qty_weekday_90;
	call symputx("qty_nwd_90", qty_nwd_90 );
run;

data dates;
	DT_INI_MES_ATUAL = PUT(&data_o.,DDMMYY10.);
	DT_FIM_MES_ATUAL = PUT(TODAY(),DDMMYY10.);
	DT_INI_90 = PUT(&data_90.,DDMMYY10.);
	DT_FIM_90 = PUT(&data_fim_90.,DDMMYY10.);
	DIAS_TT_MES_ATUAL = &dias_tt_mes_o.;
	DIAS_NUTIL_MES_ATUAL = &qty_nwd_o.;
	DIAS_TT_90 = &qty_tt_days_90.;
	DIAS_NUTIL_90 = &qty_nwd_90.;
RUN;

%put NOTE- M�s atual: &data_o.;
%put NOTE- Data 90: &data_90.;
%put NOTE- Data Expurgo: &data_ex.;

%put NOTE- Quantidade de dias n�o �teis no m�s atual: &qty_nwd_o.;
%put NOTE- Quantidade de dias n�o �teis em 90 dias: &qty_nwd_90.;


%let vnwd = 0 ;

PROC SQL NOPRINT;
SELECT DISTINCT DIAS_PROC
	INTO :vnwd separated by ','
	from nwd
	WHERE DIAS_PROC GE &data_o.;
quit;

%let qty_nwd_o = %sysevalf(&qty_nwd_o. + &sqlobs.);

PROC SQL NOPRINT;
SELECT DISTINCT DIAS_PROC
	INTO :vnwd_90 separated by ','
	from nwd
	WHERE DIAS_PROC BETWEEN &data_o. AND &data_90.;
quit;

%let qty_nwd_90 = %sysevalf(&qty_nwd_90. + &sqlobs.);

data dates_V2;
	DT_INI_MES_ATUAL = PUT(&data_o.,DDMMYY10.);
	DT_FIM_MES_ATUAL = PUT(TODAY(),DDMMYY10.);
	DT_INI_90 = PUT(&data_90.,DDMMYY10.);
	DT_FIM_90 = PUT(&data_fim_90.,DDMMYY10.);
	DIAS_TT_MES_ATUAL = &dias_tt_mes_o.;
	DIAS_NUTIL_MES_ATUAL = &qty_nwd_o.;
	DIAS_TT_90 = &qty_tt_days_90.;
	DIAS_NUTIL_90 = &qty_nwd_90.;
RUN;
PROC SORT DATA=NWD; BY DIAS_NUTIL;RUN;
proc print data=dates_V2 ;run;
proc print data=nwd; run;

%del_dataset(BOOK1);
%del_dataset(PARAM);
/*%del_dataset(METADADOS_OPFI);*/
/*%del_dataset(NWD);*/
%del_dataset(dates);